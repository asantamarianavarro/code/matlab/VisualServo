function J=Jacobian_s2J(cP,uv,f,u0,v0)
%Jacobian_s2J(cP) composes a Jacobian
% matrix from image features vector
% and depth coordinates of feature points

X=cP(1,:);Y=cP(2,:);Z=cP(3,:);

x=(uv(1,:)-u0)/f;
y=(uv(2,:)-v0)/f;
m=length(x);

for i=1:m
    J(2*i-1:2*i,1:6)=[-1/Z(i), 0, x(i)/Z(i), x(i)*y(i), -(1+x(i)^2), y(i);
       0, -1/Z(i), y(i)/Z(i), 1+y(i)^2, -x(i)*y(i), -x(i)];
end
