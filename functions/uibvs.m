% Copyright (C) 
% Author asantamaria (asantamaria@iri.upc.edu)
% All rights reserved.
% 
% This file is part of VisualServo Matlab library
% VisualServo library is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Lesser General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>
%
%   __________________________________________________________________
%
%   [Vc,T0c_new,img_str,img_x_str,f_uibvs] = uibvs(T0o,T0c_x,T0c_0,f_uibvs,f_real,u0,v0,lambda,t,Ts,diTj,cj_0,pn,alphas,noise,plot_uibvs_lyapunov)
% 
%   This function computes the uncalibrated image-based visual servo method
%   (UIBVS) from this paper:
% 
%   A. Santamaria-Navarro and J. Andrade-Cetto, 
%   Uncalibrated image-based visual servoing, 
%   IEEE International Conference on Robotics and Automation,
%   2013, Karlsruhe, pp. 5227-5232.
%    
%   Inputs:
%       - T0o:      Object pose in world frame (4x4 homogenous transform).            
%       - T0c_x:    Desired final camera pose (4x4 homogenous transform).            
%       - T0c_0:    Initial camera pose (4x4 homogenous transform).
%       - f_uibvs:   Simulated focal length.               
%       - f_real:   Real focal length.             
%       - u0:       Center of the Image (horizontal).
%       - v0:       Center of the Image (vertical).        
%       - lambda:   Proportional control gain.           
%       - t:        Time sequence.               
%       - Ts:       Time step.           
%       - diTj:     Integral condition.            
%       - cj_0:     Control points.            
%       - pn:       3D feature points.              
%       - alphas:   Alphas (aij's).            
%       - noise:    Standard deviation of image noise.            
%       - plot_uibvs_lyapunov:  Enable plot of Lyapunov stability proof.  
% 
%   Outputs:
%       - Vc:           Camera velocities.           
%       - T0c_new:      Camera pose during trajectory.           
%       - uv:           Feature's coordinates in pixels (to plot)                
%       - f_uibvs:      Simulated focal length (estimated online).     
%       - Lyap:         Lyapunov proof data.
% 
%   Copyright asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [Vc,T0c_new,uv,f_uibvs,Lyap] = uibvs(T0o,T0c_x,T0c_0,f_uibvs,f_real,u0,v0,lambda,t,Ts,diTj,cj_0,pn,alphas,noise)

disp('Computing UIBVS');

diTj_int(:,:,1)=diTj;

%% Desired features ______________________________________________

    % Base coordinates r.t Desired frame 
    cP_x = inv(T0c_x)*T0o*[cj_0;ones(1,length(cj_0))];  
       
    % Desired features vector
    [s_x]=featuresvector(cP_x);
       
%% simulating all time intervals _________________________________
for k=1:length(t)

    % Camera update r.t. world frame
    T0c_new(:,:,k)=diTj_int(:,:,k)+T0c_0;

    % Features transform r.t. camera frame
    cP=inv(T0c_new(:,:,k))*T0o*[cj_0;ones(1,length(cj_0))]; 

%% Actual features  ______________________________________________

    % Features pixel coordinates only To plot
    [uv(:,:,k)]=imageprojection(f_real,f_real,u0,v0,cP);
    [uv(:,:,k)]=[uv(:,:,k)]+noise*randn(2,length(uv(:,:,k)));
      
    [s]=featuresvector(cP);
   
%%  Visual Jacobian ______________________________________________

    % Object coordinates r.t. Camera frame with f simulated
    pj=inv(T0c_new(:,:,k))*pn;
    [uvn]=imageprojection(f_uibvs,f_uibvs,u0,v0,pj);

    % noise add
    uvn=uvn+noise*randn(2,length(uvn));
      
    % % Image plane Plot
    % figure(2);clf;axis equal;hold on; plot(uvn(1,:),uvn(2,:),'.')

    [J,~]=computeJacobian(alphas,u0,v0,uvn,cj_0,size(pn,2));
       
%% Control _______________________________________________________

    [vel,V_modified,Lyap(k,:)] = kcontrol(lambda,J,s,s_x,k);
    Vc(:,k)=vel;
    % Diferential change of T0c under Vc
    diTj(:,:,k)=T0c_new(:,:,k)*V_modified;
    % Integration aproximation
    diTj_int(:,:,k+1)=diTj_int(:,:,k)+diTj(:,:,k)*Ts;

end

end
