% Copyright (C) 
% Author asantamaria (asantamaria@iri.upc.edu)
% All rights reserved.
% 
% This file is part of VisualServo Matlab library
% VisualServo library is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Lesser General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>
%
%   __________________________________________________________________
%
%   [Vc,T0c_new,uv] = ibvs(T0o,T0c_x,T0c_0,cj_0,f_ibvs,f_real,u0,v0,lambda,t,Ts,diTj,noise)
% 
%   This function computes the uncalibrated image-based visual servo method
%   (UIBVS) from this paper:
% 
%   A. Santamaria-Navarro and J. Andrade-Cetto, 
%   Uncalibrated image-based visual servoing, 
%   IEEE International Conference on Robotics and Automation,
%   2013, Karlsruhe, pp. 5227-5232.
%    
%   Inputs:
%       - T0o:      Object pose in world frame (4x4 homogenous transform)            
%       - T0c_x:    Desired final camera pose (4x4 homogenous transform)            
%       - T0c_0:    Initial camera pose (4x4 homogenous transform)
%       - cj_0:     Control points            
%       - f_ibvs:   Focal length in case of a zoom    
%       - f_real:   Real focal length
%       - u0:       Center of the Image (horizontal)
%       - v0:       Center of the Image (vertical)        
%       - lambda:   Proportional control gain           
%       - t:        Time sequence               
%       - Ts:       Time step            
%       - diTj:     Integral condition                     
%       - noise:    Standard deviation of image noise            
% 
%   Outputs:
%       - Vc:           Camera velocities           
%       - T0c_new:      Camera pose during trajectory           
%       - uv:           Feature's coordinates in pixels (to plot)                              
% 
%   Copyright asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [Vc,T0c_new,uv] = ibvs(T0o,T0c_x,T0c_0,cj_0,f_ibvs,f_real,u0,v0,lambda,t,Ts,diTj,noise)

disp('Computing IBVS');

diTj_int(:,:,1)=diTj;

%% Desired features ______________________________________________

    % Base coordinates r.t Desired frame
    cP_x=inv(T0c_x)*T0o*[cj_0;ones(1,length(cj_0))];  
  
    % Desired features vector
    [s_x]=featuresvector(cP_x);

%% simulating all time intervals _________________________________
for k=1:length(t)
    
%   % White noise appearance    
%   r = 10*(-1 + (1+1).*rand(1));
%   f_ibvs=f_real+r;

    % Camera update r.t. world frame
    T0c_new(:,:,k)=diTj_int(:,:,k)+T0c_0;

    % Features transform r.t. camera frame
    cP=inv(T0c_new(:,:,k))*T0o*[cj_0;ones(1,length(cj_0))];
    
 %% Actual features  _____________________________________________

    % Features pixel coordinates
    [uv(:,:,k)]=imageprojection(f_real,f_real,u0,v0,cP);
    [uv(:,:,k)]=[uv(:,:,k)]+noise*randn(2,length(uv(:,:,k)));
    
    [s]=featuresvector(cP);
    
%% Control _______________________________________________________

    % Visual Jacobian matrix in desired features
    J=Jacobian_s2J(cP,uv(:,:,k),f_ibvs,u0,v0);   
    % Jacobian pseudo-inverse
    J_pseudo=pinv(J);
    % Proportional control
    Vc(:,k)=-lambda*J_pseudo*(s-s_x);
    vel=Vc(:,k);
    % Lineal Velocity
    v_linear(1:3,1)=Vc(1:3,k);
    % Angular Velocity
    v_angular(1:3,1)=Vc(4:6,k);
    a=v_angular;
    % Skew-symmetric matrix 
    skew_v_angular= [0 -a(3) a(2) ; a(3) 0 -a(1); -a(2) a(1) 0];

    % Using modified velocity matrix 
    % X_dot=v_angular*X+v_linear
    V_modified=[skew_v_angular, v_linear;0 0 0 0];

    Vc(:,k)=vel;
    % Diferential change of T0c under Vc
    diTj(:,:,k)=T0c_new(:,:,k)*V_modified;
    % Integration aproximation
    diTj_int(:,:,k+1)=diTj_int(:,:,k)+diTj(:,:,k)*Ts;
    
end

end