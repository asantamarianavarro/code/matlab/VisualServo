% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MatlabPlot_constr_task_opt. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with MatlabPlot_constr_task_opt.  If not, see <http://www.gnu.org/licenses/>.

function [hleg] = generic_figure_params(hfig,title,fontsize,pos)

hleg = legend(gca,'show');
set(hleg,'Interpreter','latex','FontSize',fontsize,'Location','east');

set(hfig,'NumberTitle','off','Name',title,'Renderer','Painters','defaulttextinterpreter','latex');
set(gca,'FontSize',fontsize,'box','off','layer','top','TickDir','out','defaulttextinterpreter','latex')
set(gcf,'Color',[1,1,1])
set(findall(gcf,'-property','FontSize'),'FontSize',fontsize)
set(findall(gcf,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex')

if ~isempty(pos)
    set(hfig, 'Position', pos);
end

set(gca,'LooseInset',get(gca,'TightInset')+0.2*get(gca,'TightInset'));

grid ON
% axis tight

hax = gca;
xval = get(hax,'XLim');
yval = get(hax,'YLim');
rectangle('Position',[xval(1) yval(1) xval(2)-xval(1) yval(2)-yval(1)],'EdgeColor',[0.8 0.8 0.8]);
set(hax,'GridLineStyle','--');


return