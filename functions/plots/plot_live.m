% Copyright (C) 
% Author asantamaria (asantamaria@iri.upc.edu)
% All rights reserved.
% 
% This file is part of VisualServo Matlab library
% VisualServo library is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Lesser General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>
%
%   __________________________________________________________________
%
%   Function to plot "live" all parameters from Visual Servo. 
% 
%   Copyright asantamaria@iri.upc.edu.
%   __________________________________________________________________

function plot_live(t,uv_des,uv_ibvs,uv_uibvs,uv_pbvs,Vc_ibvs,Vc_uibvs,Vc_pbvs,...
                  c_o,Tc_ibvs,pn,cj_0,c,Tc_uibvs,c_po,Tc_pbvs,ibvs_epose,uibvs_epose,pbvs_epose,plot_opts)

no = size(uv_ibvs,2);
to = length(t);

x_o = ibvs_epose(1,:);
y_o = ibvs_epose(2,:);
z_o = ibvs_epose(3,:);
roll_o = ibvs_epose(4,:);
pitch_o = ibvs_epose(5,:);
yaw_o = ibvs_epose(6,:);

x = uibvs_epose(1,:);
y = uibvs_epose(2,:);
z = uibvs_epose(3,:);
roll = uibvs_epose(4,:);
pitch = uibvs_epose(5,:);
yaw = uibvs_epose(6,:);

x_po = pbvs_epose(1,:);
y_po = pbvs_epose(2,:);
z_po = pbvs_epose(3,:);
roll_po = pbvs_epose(4,:);
pitch_po = pbvs_epose(5,:);
yaw_po = pbvs_epose(6,:);

%% POINTS TRAJECTORY %%%%%%%

if plot_opts.imgplane_2Dtraj

figure(1);
axis equal
axis('ij')
xlabel('u (pixels)','fontsize',20), ylabel('v (pixels)','fontsize',20)
set(gca,'FontSize',18)
set(gcf,'Color',[1,1,1])
set(1,'Name','Points trajectories','NumberTitle','off') 
title('Image plane: Feature trajectories','fontsize',24,'Color', 'k');

ini_x=[];
ini_y=[];
fi_x=[];
fi_y=[];
pb1_x=[];
pb1_y=[];
pb2_x=[];
pb2_y=[];
pb3_x=[];
pb3_y=[];
pb4_x=[];
pb4_y=[];

pr1_x=[];
pr1_y=[];
pr2_x=[];
pr2_y=[];
pr3_x=[];
pr3_y=[];
pr4_x=[];
pr4_y=[];
    
pk1_x=[];
pk1_y=[];
pk2_x=[];
pk2_y=[];
pk3_x=[];
pk3_y=[];
pk4_x=[];
pk4_y=[];

for i=1:no
    ini_x=[ini_x;uv_ibvs(1,i,1)];
    ini_y=[ini_y;uv_ibvs(2,i,1)];
    fi_x=[fi_x;uv_des(1,i)];
    fi_y=[fi_y;uv_des(2,i)];
end

figure(1), hold on, g1p1=plot(ini_x,ini_y,'ko','MarkerSize',10,'LineWidth',2);    
figure(1), hold on, g1p4=plot(fi_x,fi_y,'gx','MarkerSize',10,'LineWidth',2);    

for q=1:to
    pb1_x=[pb1_x;uv_ibvs(1,1,q)];
    pb1_y=[pb1_y;uv_ibvs(2,1,q)];
    pb2_x=[pb2_x;uv_ibvs(1,2,q)];
    pb2_y=[pb2_y;uv_ibvs(2,2,q)];
    pb3_x=[pb3_x;uv_ibvs(1,3,q)];
    pb3_y=[pb3_y;uv_ibvs(2,3,q)];
    pb4_x=[pb4_x;uv_ibvs(1,4,q)];
    pb4_y=[pb4_y;uv_ibvs(2,4,q)];
        
    pr1_x=[pr1_x;uv_uibvs(1,1,q)];
    pr1_y=[pr1_y;uv_uibvs(2,1,q)];
    pr2_x=[pr2_x;uv_uibvs(1,2,q)];
    pr2_y=[pr2_y;uv_uibvs(2,2,q)];
    pr3_x=[pr3_x;uv_uibvs(1,3,q)];
    pr3_y=[pr3_y;uv_uibvs(2,3,q)];
    pr4_x=[pr4_x;uv_uibvs(1,4,q)];
    pr4_y=[pr4_y;uv_uibvs(2,4,q)];

    pk1_x=[pk1_x;uv_pbvs(1,1,q)];
    pk1_y=[pk1_y;uv_pbvs(2,1,q)];
    pk2_x=[pk2_x;uv_pbvs(1,2,q)];
    pk2_y=[pk2_y;uv_pbvs(2,2,q)];
    pk3_x=[pk3_x;uv_pbvs(1,3,q)];
    pk3_y=[pk3_y;uv_pbvs(2,3,q)];
    pk4_x=[pk4_x;uv_pbvs(1,4,q)];
    pk4_y=[pk4_y;uv_pbvs(2,4,q)];
end
    
xminibvs = min(min(min(uv_ibvs(1,:,:))));
xmaxibvs = max(max(max(uv_ibvs(1,:,:))));
yminibvs = min(min(min(uv_ibvs(2,:,:))));
ymaxibvs = max(max(max(uv_ibvs(2,:,:))));

xminuibvs = min(min(min(uv_uibvs(1,:,:))));
xmaxuibvs = max(max(max(uv_uibvs(1,:,:))));
yminuibvs = min(min(min(uv_uibvs(2,:,:))));
ymaxuibvs = max(max(max(uv_uibvs(2,:,:))));

xminpbvs = min(min(min(uv_pbvs(1,:,:))));
xmaxpbvs = max(max(max(uv_pbvs(1,:,:))));
yminpbvs = min(min(min(uv_pbvs(2,:,:))));
ymaxpbvs = max(max(max(uv_pbvs(2,:,:))));

xmin = min(min(xminibvs,xminuibvs),xminpbvs);
xmax = max(max(xmaxibvs,xmaxuibvs),xmaxpbvs);
ymin = min(min(yminibvs,yminuibvs),yminpbvs);
ymax = max(max(ymaxibvs,ymaxuibvs),ymaxpbvs);

axis([xmin-5 xmax+5 ymin-5 ymax+5]);
    
    
end
%% IBVS: FEATURES ERROR ___________________________________________________
if plot_opts.feat_error
figure(2);    
title('IBVS: Features error','fontsize',20,'Color', 'k') 
ylabel('error (pixels)','fontsize',20)
xlabel('time (s)','fontsize',20)
legend('Boxoff'), set(gca,'FontSize',20)
set(gcf,'Color',[1,1,1])
set(2,'Name','IBVS: Features error','NumberTitle','off');

n=4;
s = reshape(uv_ibvs,n*2,to)';
s_x = repmat(reshape(uv_des,n*2,1)',to,1);

for i=1:n*2
    e(:,i)=s(:,i)-s_x(:,i);
end

miny = 0;
maxy = 0;
for ii = 1:8
    if e(1,ii) < miny miny = e(1,ii); end
    if e(1,ii) > maxy maxy = e(1,ii); end
end
axis([t(1) t(end) miny maxy]);

end
%% UIBVS: FEATURES ERROR __________________________________________________
if plot_opts.feat_error
figure(3);    
title('UIBVS: Features error','fontsize',20,'Color', 'k') 
ylabel('error (pixels)','fontsize',20)
xlabel('time (s)','fontsize',20)
legend('Boxoff'), set(gca,'FontSize',20)
set(gcf,'Color',[1,1,1])
set(3,'Name','UIBVS: Features error','NumberTitle','off');

s2 = reshape(uv_uibvs,n*2,to)';
s2_x = repmat(reshape(uv_des,n*2,1)',to,1);

for i=1:n*2
    e2(:,i)=s2(:,i)-s2_x(:,i);
end

miny = 0;
maxy = 0;
for ii = 1:8
    if e2(1,ii) < miny miny = e2(1,ii); end
    if e2(1,ii) > maxy maxy = e2(1,ii); end
end
axis([t(1) t(end) miny maxy]);

end
%% PBVS: FEATURES ERROR ___________________________________________________
if plot_opts.feat_error
figure(4);    
title('PBVS: Features error','fontsize',20,'Color', 'k') 
ylabel('error (pixels)','fontsize',20)
xlabel('time (s)','fontsize',20)
legend('Boxoff'), set(gca,'FontSize',20)
set(gcf,'Color',[1,1,1])
set(4,'Name','PBVS: Features error','NumberTitle','off');

s3 = reshape(uv_pbvs,n*2,to)';
s3_x = repmat(reshape(uv_des,n*2,1)',to,1);

for i=1:n*2
    e3(:,i)=s3(:,i)-s3_x(:,i);
end

miny = 0;
maxy = 0;
for ii = 1:8
    if e3(1,ii) < miny miny = e3(1,ii); end
    if e3(1,ii) > maxy maxy = e3(1,ii); end
end
axis([t(1) t(end) miny maxy]);

end

%% IBVS: CAM TRAJECTORY ___________________________________________________
if plot_opts.campose_error
figure(5);
title('IBVS: Camera 3D trajectory','fontsize',20,'Color', 'k') 
axis equal
axis([-0.5 2.5 -2.5 1 -0.5 1.5]);
grid on
xlabel('x (m)','fontsize',20), ylabel('y (m)','fontsize',20)
zlabel('z (m)','fontsize',20)%, title('Orig-IBVS Camera trajectory (m)','fontsize',24,'Color', 'k')
set(gca,'FontSize',18)
set(gcf,'Color',[1,1,1])
set(5,'Name','IBVS: Camera 3D trajectory','NumberTitle','off');

figure(5), hold on, 
g5p1=plot3(pn(1,:),pn(2,:),pn(3,:),'bo','LineWidth',2,'MarkerSize',10); 
g5p2=plot3(cj_0(1,:),cj_0(2,:),cj_0(3,:),'rx','LineWidth',2,'MarkerSize',10);
figure(5), hold on, plot(ht(Tc_ibvs(:,:,1)),'START'),...
    plot(c_o,ht(Tc_ibvs(:,:,1))),...
    plot(ht(Tc_uibvs(:,:,end)),'END'),...
    plot(c_o,ht(Tc_uibvs(:,:,end)));
end
%% UIBVS: CAM TRAJECTORY __________________________________________________
if plot_opts.campose_error
figure(6);
title('UIBVS: Camera 3D trajectory','fontsize',20,'Color', 'k') 
axis equal
axis([-0.5 2.5 -2.5 1 -0.5 1.5]);
grid on
xlabel('x (m)','fontsize',20), ylabel('y (m)','fontsize',20)
zlabel('z (m)','fontsize',20)%, title('Orig-IBVS Camera trajectory (m)','fontsize',24,'Color', 'k')
set(gca,'FontSize',18)
set(gcf,'Color',[1,1,1])
set(6,'Name','UIBVS: Camera 3D trajectory','NumberTitle','off');

figure(6), hold on, 
g6p1=plot3(pn(1,:),pn(2,:),pn(3,:),'bo','LineWidth',2,'MarkerSize',10); 
g6p2=plot3(cj_0(1,:),cj_0(2,:),cj_0(3,:),'rx','LineWidth',2,'MarkerSize',10);
figure(6), hold on, plot(ht(Tc_uibvs(:,:,1)),'START'),...
    plot(c,ht(Tc_uibvs(:,:,1))),...
    plot(ht(Tc_uibvs(:,:,end)),'END'),...
    plot(c,ht(Tc_uibvs(:,:,end)));
end
%% PBVS: CAM TRAJECTORY ___________________________________________________
if plot_opts.campose_error
figure(7);
title('PBVS: Camera 3D trajectory','fontsize',20,'Color', 'k') 
axis equal
axis([-0.5 2.5 -2.5 1 -0.5 1.5]);
grid on
xlabel('x (m)','fontsize',20), ylabel('y (m)','fontsize',20)
zlabel('z (m)','fontsize',20)%, title('Orig-IBVS Camera trajectory (m)','fontsize',24,'Color', 'k')
set(gca,'FontSize',18)
set(gcf,'Color',[1,1,1])
set(7,'Name','PBVS: Camera 3D trajectory','NumberTitle','off');

figure(7), hold on, 
g7p1=plot3(pn(1,:),pn(2,:),pn(3,:),'bo','LineWidth',2,'MarkerSize',10); 
g7p2=plot3(cj_0(1,:),cj_0(2,:),cj_0(3,:),'rx','LineWidth',2,'MarkerSize',10);
figure(7), hold on, plot(ht(Tc_pbvs(:,:,1)),'START'),...
    plot(c_po,ht(Tc_pbvs(:,:,1))),...
    plot(ht(Tc_pbvs(:,:,end)),'END'),...
    plot(c_po,ht(Tc_pbvs(:,:,end)));
end
%% IBVS: CAM POSE ERROR ___________________________________________________
if plot_opts.cam_3Dtraj
figure(8); 
legend('Boxoff')
ylabel('error (m;rad)','fontsize',20)
xlabel('time (s)','fontsize',20)
title('IBVS: Position and Orientation Error','fontsize',20,'Color', 'k')
set(gcf,'Color',[1,1,1])
set(gca,'FontSize',20)
set(8,'Name','IBVS: Position and Orientation Error','NumberTitle','off');  
miny = min(min(min(min(min(x_o(1),y_o(1)),z_o(1)),yaw_o(1)),pitch_o(1)),roll_o(1));
maxy = max(max(max(max(max(x_o(1),y_o(1)),z_o(1)),yaw_o(1)),pitch_o(1)),roll_o(1));
axis([t(1) t(end) miny maxy]);
end
%% UIBVS: CAM POSE ERROR __________________________________________________
if plot_opts.cam_3Dtraj
figure(9); 
legend('Boxoff')
ylabel('error (m;rad)','fontsize',20)
xlabel('time (s)','fontsize',20)
title('UIBVS: Position and Orientation Error','fontsize',20,'Color', 'k')
set(gcf,'Color',[1,1,1])
set(gca,'FontSize',20)
set(9,'Name','UIBVS: Position and Orientation Error','NumberTitle','off');  
miny = min(min(min(min(min(x(1),y(1)),z(1)),yaw(1)),pitch(1)),roll(1));
maxy = max(max(max(max(max(x(1),y(1)),z(1)),yaw(1)),pitch(1)),roll(1));
axis([t(1) t(end) miny maxy]);
end
%% PBVS: CAM POSE ERROR ___________________________________________________
if plot_opts.cam_3Dtraj
figure(10); 
legend('Boxoff')
ylabel('error (m;rad)','fontsize',20)
xlabel('time (s)','fontsize',20)
title('PBVS: Position and Orientation Error','fontsize',20,'Color', 'k')
set(gcf,'Color',[1,1,1])
set(gca,'FontSize',20)
set(10,'Name','PBVS: Position and Orientation Error','NumberTitle','off');  
miny = min(min(min(min(min(x_po(1),y_po(1)),z_po(1)),yaw_po(1)),pitch_po(1)),roll_po(1));
maxy = max(max(max(max(max(x_po(1),y_po(1)),z_po(1)),yaw_po(1)),pitch_po(1)),roll_po(1));
axis([t(1) t(end) miny maxy]);
end
%% IBVS: CAM VELOCITY _____________________________________________________
if plot_opts.cam_vel
figure(11); 
ylabel('Velocity (m/s; rad)','fontsize',20)
xlabel('time (s)','fontsize',20)
title('IBVS: Camera velocity','fontsize',20,'Color', 'k')
set(gca,'FontSize',20)
set(gcf,'Color',[1,1,1])
set(11,'Name','IBVS: Camera velocity','NumberTitle','off');

vo = Vc_ibvs(1:3,:);
wo = Vc_ibvs(4:6,:);

miny = 0;
maxy = 0;
for ii = 1:3
    if vo(ii,1) < miny miny = vo(ii,1); end
    if wo(ii,1) < miny miny = wo(ii,1); end
    if vo(ii,1) > maxy maxy = vo(ii,1); end
    if wo(ii,1) > maxy maxy = wo(ii,1); end
end
axis([t(1) t(end) miny maxy]);
end
%% UIBVS: CAM VELOCITY ____________________________________________________
if plot_opts.cam_vel
figure(12)
ylabel('Velocity (m/s; rad)','fontsize',20)
xlabel('time (s)','fontsize',20)
title('UIBVS: Camera velocity','fontsize',20,'Color', 'k')
set(gca,'FontSize',20)
set(gcf,'Color',[1,1,1])
set(12,'Name','UIBVS: Camera velocity','NumberTitle','off');

v = Vc_uibvs(1:3,:);
w = Vc_uibvs(4:6,:);

miny = 0;
maxy = 0;
for ii = 1:3
    if v(ii,1) < miny miny = v(ii,1); end
    if w(ii,1) < miny miny = w(ii,1); end
    if v(ii,1) > maxy maxy = v(ii,1); end
    if w(ii,1) > maxy maxy = w(ii,1); end
end
axis([t(1) t(end) miny maxy]);
end
%% PBVS: CAM VELOCITY _____________________________________________________
if plot_opts.cam_vel
figure(13); 
ylabel('Velocity (m/s; rad)','fontsize',20)
xlabel('time (s)','fontsize',20)
title('PBVS: Camera velocity','fontsize',20,'Color', 'k')
set(gca,'FontSize',20)
set(gcf,'Color',[1,1,1])
set(13,'Name','PBVS: Camera velocity','NumberTitle','off');

vpo = Vc_pbvs(1:3,:);
wpo = Vc_pbvs(4:6,:);

miny = 0;
maxy = 0;
for ii = 1:3
    if vpo(ii,1) < miny miny = vpo(ii,1); end
    if wpo(ii,1) < miny miny = wpo(ii,1); end
    if vpo(ii,1) > maxy maxy = vpo(ii,1); end
    if wpo(ii,1) > maxy maxy = wpo(ii,1); end
end
axis([t(1) t(end) miny maxy]);
end


%% PLOTS  _________________________________________________________________

for i=1:to
%     pause(0.1)

% plot points traj ________________________________________________________
if plot_opts.imgplane_2Dtraj
figure(1), hold on, 
g1p2=plot(pb1_x(i),pb1_y(i),'b.','LineWidth',3); 
   plot(pb2_x(i),pb2_y(i),'b.','LineWidth',3),...
   plot(pb3_x(i),pb3_y(i),'b.','LineWidth',3),...
   plot(pb4_x(i),pb4_y(i),'b.','LineWidth',3),...
g1p3=plot(pr1_x(i),pr1_y(i),'r.','LineWidth',3);
   plot(pr2_x(i),pr2_y(i),'r.','LineWidth',3),...
   plot(pr3_x(i),pr3_y(i),'r.','LineWidth',3),...
   plot(pr4_x(i),pr4_y(i),'r.','LineWidth',3);
g1p4=plot(pk1_x(i),pk1_y(i),'k.','LineWidth',3);
   plot(pk2_x(i),pk2_y(i),'k.','LineWidth',3),...
   plot(pk3_x(i),pk3_y(i),'k.','LineWidth',3),...
   plot(pk4_x(i),pk4_y(i),'k.','LineWidth',3);
end

% plot feature error ______________________________________________________
if plot_opts.feat_error
figure(2), hold on, 
g2p1=plot(t(i),e(i,1),'Color',[0.8,0.2,0.1],'LineWidth',3);
g2p2=plot(t(i),e(i,2),'c.-','LineWidth',3);
g2p3=plot(t(i),e(i,3),'m.-','Linewidth',3);
g2p4=plot(t(i),e(i,4),'y.-','Linewidth',3);
g2p5=plot(t(i),e(i,5),'k.-','Linewidth',3);
g2p6=plot(t(i),e(i,6),'r.-','Linewidth',3);
g2p7=plot(t(i),e(i,7),'g.-','Linewidth',3);
g2p8=plot(t(i),e(i,8),'b.-','Linewidth',3);

figure(3), hold on, 
g3p1=plot(t(i),e2(i,1),'Color',[0.8,0.2,0.1],'LineWidth',3);
g3p2=plot(t(i),e2(i,2),'c.-','LineWidth',3);
g3p3=plot(t(i),e2(i,3),'m.-','Linewidth',3);
g3p4=plot(t(i),e2(i,4),'y.-','Linewidth',3);
g3p5=plot(t(i),e2(i,5),'k.-','Linewidth',3);
g3p6=plot(t(i),e2(i,6),'r.-','Linewidth',3);
g3p7=plot(t(i),e2(i,7),'g.-','Linewidth',3);
g3p8=plot(t(i),e2(i,8),'b.-','Linewidth',3);

figure(4), hold on, 
g4p1=plot(t(i),e3(i,1),'Color',[0.8,0.2,0.1],'LineWidth',3);
g4p2=plot(t(i),e3(i,2),'c.-','LineWidth',3);
g4p3=plot(t(i),e3(i,3),'m.-','Linewidth',3);
g4p4=plot(t(i),e3(i,4),'y.-','Linewidth',3);
g4p5=plot(t(i),e3(i,5),'k.-','Linewidth',3);
g4p6=plot(t(i),e3(i,6),'r.-','Linewidth',3);
g4p7=plot(t(i),e3(i,7),'g.-','Linewidth',3);
g4p8=plot(t(i),e3(i,8),'b.-','Linewidth',3);
end

% plot camera trajectory __________________________________________________
if plot_opts.campose_error
if (i<to-2)
figure(5), hold on, plot(ht(Tc_ibvs(:,:,i+1)));
end

if (i<to-2)
figure(6), hold on, plot(ht(Tc_uibvs(:,:,i+1)));
end

if (i<to-2)
figure(7), hold on, plot(ht(Tc_pbvs(:,:,i+1)));
end
end

% plot camera pose
if plot_opts.cam_3Dtraj
figure(8), hold on, 
g8p1=plot(t(i),x_o(i),'r.-','LineWidth',3);
g8p2=plot(t(i),y_o(i),'g.-','LineWidth',3);
g8p3=plot(t(i),z_o(i),'b.-','LineWidth',3);
g8p4=plot(t(i),yaw_o(i),'c.-','LineWidth',3);
g8p5=plot(t(i),pitch_o(i),'m.-','LineWidth',3);
g8p6=plot(t(i),roll_o(i),'k.-','LineWidth',3);

figure(9), hold on, 
g9p1=plot(t(i),x(i),'r.-','LineWidth',3);
g9p2=plot(t(i),y(i),'g.-','LineWidth',3);
g9p3=plot(t(i),z(i),'b.-','LineWidth',3);
g9p4=plot(t(i),yaw(i),'c.-','LineWidth',3);
g9p5=plot(t(i),pitch(i),'m.-','LineWidth',3);
g9p6=plot(t(i),roll(i),'k.-','LineWidth',3);

figure(10), hold on, 
g10p1=plot(t(i),x_po(i),'r.-','LineWidth',3);
g10p2=plot(t(i),y_po(i),'g.-','LineWidth',3);
g10p3=plot(t(i),z_po(i),'b.-','LineWidth',3);
g10p4=plot(t(i),yaw_po(i),'c.-','LineWidth',3);
g10p5=plot(t(i),pitch_po(i),'m.-','LineWidth',3);
g10p6=plot(t(i),roll_po(i),'k.-','LineWidth',3);
end

% plot velocities
if plot_opts.cam_vel
figure(11), hold on,
g11p1=plot(t(i),vo(1,i),'r.-','LineWidth',3);
g11p2=plot(t(i),vo(2,i),'g.-','LineWidth',3);
g11p3=plot(t(i),vo(3,i),'b.-','LineWidth',3);
g11p4=plot(t(i),wo(1,i),'c.-','LineWidth',3);
g11p5=plot(t(i),wo(2,i),'m.-','LineWidth',3);
g11p6=plot(t(i),wo(3,i),'k.-','LineWidth',3);

figure(12), hold on,
g12p1=plot(t(i),v(1,i),'r.-','LineWidth',3);
g12p2=plot(t(i),v(2,i),'g.-','LineWidth',3);
g12p3=plot(t(i),v(3,i),'b.-','LineWidth',3);
g12p4=plot(t(i),w(1,i),'c.-','LineWidth',3);
g12p5=plot(t(i),w(2,i),'m.-','LineWidth',3);
g12p6=plot(t(i),w(3,i),'k.-','LineWidth',3);

figure(13), hold on,
g13p1=plot(t(i),vpo(1,i),'r.-','LineWidth',3);
g13p2=plot(t(i),vpo(2,i),'g.-','LineWidth',3);
g13p3=plot(t(i),vpo(3,i),'b.-','LineWidth',3);
g13p4=plot(t(i),wpo(1,i),'c.-','LineWidth',3);
g13p5=plot(t(i),wpo(2,i),'m.-','LineWidth',3);
g13p6=plot(t(i),wpo(3,i),'k.-','LineWidth',3);
end

%% Legends ________________________________________________________________
if (i==2)

if plot_opts.imgplane_2Dtraj    
figure(1),hold on
h1=legend([g1p2 g1p3 g1p4 g1p1 g1p4],'IBVS','UIBVS','PBVS','Start','End');
set(h1,'fontsize',20);
%legend('Boxoff');
hold off;
end

if plot_opts.feat_error
figure(2), hold on
h2=legend([g2p1 g2p2 g2p3 g2p4 g2p5 g2p6 g2p7 g2p8],'u_1','u_2','u_3','u_4','v_1','v_2','v_3','v_4');
set(h2,'fontsize',20);
%legend('Boxoff');
hold off;
figure(3), hold on
h3=legend([g3p1 g3p2 g3p3 g3p4 g3p5 g3p6 g3p7 g3p8],'u_1','u_2','u_3','u_4','v_1','v_2','v_3','v_4');
set(h3,'fontsize',20);
%legend('Boxoff');
hold off;
figure(4), hold on
h4=legend([g4p1 g4p2 g4p3 g4p4 g4p5 g4p6 g4p7 g4p8],'u_1','u_2','u_3','u_4','v_1','v_2','v_3','v_4');
set(h4,'fontsize',20);
%legend('Boxoff');
hold off;
end

if plot_opts.campose_error
figure(5), hold on
h5=legend([g5p1 g5p2],'Original points','Base Points (Features)');
set(h5,'fontsize',20);
%legend('Boxoff');
hold off;
figure(6), hold on
h6=legend([g6p1 g6p2],'Original points','Base Points (Features)');
set(h6,'fontsize',20);
%legend('Boxoff');
hold off;
figure(7), hold on
h7=legend([g7p1 g7p2],'Original points','Base Points (Features)');
set(h7,'fontsize',20);
%legend('Boxoff');
hold off;
end

if plot_opts.cam_3Dtraj
figure(8), hold on
h8=legend([g8p1 g8p2 g8p3 g8p4 g8p5 g8p6],'x','y','z','\psi','\theta','\phi');
set(h8,'fontsize',20);
%legend('Boxoff');
hold off;
figure(9), hold on
h9=legend([g9p1 g9p2 g9p3 g9p4 g9p5 g9p6],'x','y','z','\psi','\theta','\phi');
set(h9,'fontsize',20);
%legend('Boxoff');
hold off;
figure(10), hold on
h10=legend([g10p1 g10p2 g10p3 g10p4 g10p5 g10p6],'x','y','z','\psi','\theta','\phi');
set(h10,'fontsize',20);
%legend('Boxoff');
hold off;
end

if plot_opts.cam_vel
figure(11), hold on
h11=legend([g11p1 g11p2 g11p3 g11p4 g11p5 g11p6],'\nu_x','\nu_y','\nu_z','\omega_x','\omega_y','\omega_z');
set(h11,'fontsize',20);
%legend('Boxoff');
hold off;
figure(12), hold on
h12=legend([g12p1 g12p2 g12p3 g12p4 g12p5 g12p6],'\nu_x','\nu_y','\nu_z','\omega_x','\omega_y','\omega_z');
set(h12,'fontsize',20);
%legend('Boxoff');
hold off;
figure(13), hold on
h13=legend([g13p1 g13p2 g13p3 g13p4 g13p5 g13p6],'\nu_x','\nu_y','\nu_z','\omega_x','\omega_y','\omega_z');
set(h13,'fontsize',20);
%legend('Boxoff');
hold off;
end

distFig('Screen','C','Not',[5,6,7]);
% pause(60);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

drawnow

end
disp('END');





end